import { InMemoryDbService } from 'angular-in-memory-web-api';

export class TestData implements InMemoryDbService {
  createDb() {
    let employeeDetails = [
      { id: 1, firstName: 'Krishna', lastName: 'C', age:22, email: 'krishanc@gmail.com', active: false },
      { id: 2, firstName: 'Vishnu', lastName: 'R', age:23, email: 'vishnur@gmail.com', active: false },
      { id: 3, firstName: 'Rama', lastName: 'B', age:24, email: 'ramab@gmail.com', active: false }
    ];
    return { employees: employeeDetails };
  }
}