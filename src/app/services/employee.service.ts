import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { Employee } from '../models/employee';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {
    //URL for CRUD operations
    employeeUrl = "/api/employees";
    //Create constructor to get Http instance
    constructor(private http: HttpClient) { }
    //Fetch all employees
    getAllEmployees(): Observable<Employee[]> {
        return this.http.get<Employee[]>(this.employeeUrl).pipe(
            tap(employees => console.log("Number of employees: " + employees.length)),
            catchError(this.handleError)
        );
    }
    //Create Employee
    createEmployee(employee: Employee): Observable<number> {
        let httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.post<Employee>(this.employeeUrl + "/" + employee.id, employee, {
            headers: httpHeaders,
            observe: 'response'
        }
        ).pipe(
            map(res => res.status),
            catchError(this.handleError)
        );
    }
    //Fetch employee by id
    getEmployeeById(employeeId: string): Observable<Employee> {
        return this.http.get<Employee>(this.employeeUrl + "/" + employeeId).pipe(
            tap(employee => console.log(employee.firstName + " " + employee.lastName)),
            catchError(this.handleError)
        );
    }
    //Update employee
    updateEmployee(employee: Employee): Observable<number> {
        let httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.put<Employee>(this.employeeUrl + "/" + employee.id, employee, {
            headers: httpHeaders,
            observe: 'response'
        }
        ).pipe(
            map(res => res.status),
            catchError(this.handleError)
        );   
    }
    //Delete employee	
    deleteEmployeeById(employeeId: string): Observable<number> {
        return this.http.delete<number>(this.employeeUrl + "/" + employeeId).pipe(
            tap(status => console.log("status: " + status)),
            catchError(this.handleError)
        );
    }
    private handleError(error: any) {
        console.error(error);
        return throwError(error);
    }
}