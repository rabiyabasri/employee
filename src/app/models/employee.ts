export class Employee {
    id: number;
    firstName: string;
    lastName: string;
    age: number; 
    email: string;
    active: boolean;
} 