import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee';
// This lets me use jquery
declare var $: any;
@Component({
	selector: 'app-employee',
	templateUrl: './employee.component.html',
	styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
	//Component properties
	allEmployees: Employee[];
	statusCode: number;
	requestProcessing = false;
	employeeIdToUpdate = null;
	processValidation = false;
	employeeForm: FormGroup;

	deleteId: string;
	
	//Create constructor to get service instance
	constructor(private employeeService: EmployeeService,  private formBuilder: FormBuilder) {
	}
	//Create ngOnInit() and and load employees
	ngOnInit(): void {
		this.getAllEmployees();
		//Create form
		this.employeeForm = this.formBuilder.group({
			firstName: ['', Validators.required],
			lastName: ['', Validators.required],
			age: ['', [Validators.required, Validators.max(110)]],
			email: ['', [Validators.required, Validators.email]]
		});
	}
	showModal():void {
		$("#myModal").modal('show');
	  }
		sendModal(): void {
		  //do something here
		  this.hideModal();
		}
		hideModal():void {
		  document.getElementById('close-modal').click();
		}
	//Fetch all employees
	getAllEmployees() {
		this.employeeService.getAllEmployees()
			.subscribe(
				data => this.allEmployees = data,
				errorCode => this.statusCode = errorCode);
	}
	//Handle create and update employee
	onEmployeeFormSubmit() {
		this.processValidation = true;
		if (this.employeeForm.invalid) {
			return; //Validation failed, exit from method.
		}
		//Form is valid, now perform create or update
		this.preProcessConfigurations();
		let employee = this.employeeForm.value;
		if (this.employeeIdToUpdate === null) {
			//Generate employee id then create employee
			this.employeeService.getAllEmployees()
				.subscribe(employees => {
					//Generate employee id (logic is for demo)	 
					let maxIndex = employees.length - 1;
					let employeeWithMaxIndex = employees[maxIndex];
					let employeeId = employeeWithMaxIndex.id + 1;
					employee.id = employeeId;

					//Create employee
					this.employeeService.createEmployee(employee)
						.subscribe(statusCode => {
							//Expecting success code 201 from server
							this.statusCode = statusCode;
							this.getAllEmployees();
							this.backToCreateEmployee();
						},
							errorCode => this.statusCode = errorCode
						);
				});
		} else {
			//Handle update employee
			employee.id = this.employeeIdToUpdate;
			this.employeeService.updateEmployee(employee)
				.subscribe(statusCode => {
					//this.statusCode = statusCode;
					//Expecting success code 204 from server
					this.statusCode = 200;
					this.getAllEmployees();
					this.backToCreateEmployee();
				},
					errorCode => this.statusCode = errorCode);
		}
		$("#myModal").modal('hide');
	}
	get f() { return this.employeeForm.controls; }
	
	
	//Load employee by id to edit
	loadEmployeeToEdit(employeeId: string) {
		$("#myModal").modal('show');
		this.preProcessConfigurations();
		this.employeeService.getEmployeeById(employeeId)
			.subscribe(employee => {
				this.employeeIdToUpdate = employee.id;
				this.employeeForm.setValue({ firstName: employee.firstName, lastName: employee.lastName, age:employee.age, email:employee.email });
				this.processValidation = true;
				this.requestProcessing = false;
			},
				errorCode => this.statusCode = errorCode);
	}
	showDeleteModal(selectedId: string) {
		$("#deleteModal").modal('show');
		this.deleteId = selectedId;
		//this.deleteEmployee(selectedId);
	}
	
	//Delete employee
	deleteEmpployee() {
		this.preProcessConfigurations();
		this.employeeService.deleteEmployeeById(this.deleteId)
			.subscribe(successCode => {
				//this.statusCode = successCode;
				//Expecting success code 204 from server
				this.statusCode = 204;
				this.getAllEmployees();
				this.backToCreateEmployee();
			},
			errorCode => {
				this.statusCode = errorCode
			});
			$("#deleteModal").modal('hide');
	}
	public selectEmployee(event: any, employee: any) {
		employee.active = !employee.active;
	}
	//Perform preliminary processing configurations
	preProcessConfigurations() {
		this.statusCode = null;
		this.requestProcessing = true;
	}
	//Go back from update to create
	backToCreateEmployee() {
		this.employeeIdToUpdate = null;
		this.employeeForm.reset();
		this.processValidation = false;
	}
}
